/*
	THE FOLLOWING FILE CONTAINS ALL THE FUNCTION FOR
	- filtering the image and get gradient values (X and Y)
*/

#pragma once 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define MASK_RADIUS 1
#define MASK_SIZE MASK_RADIUS * 2 + 1
#define PI 3.14159265358979323846
#define TILE 32 
#define TILE_SIZE (TILE + MASK_SIZE - 1)

char kernel[3] = {-1, 0, 1};

/*
    For computing the gradient we use the operation of convolution 
    What we want to obtain the gradient along the x and y axis. 
    input -> image in grayscale colors 
    output -> table with filtred values 

    We repeat the algorithm 2 times because we want to calculate the values for 
    x gradient -> kernel = [-1, 0, 1]
    y gradient -> kernel = [-1,
                            0,
                            1]
*/

//******************************************** CPU ********************************************// 
// Horizontal convolution 
void convolution2DH (unsigned char *data, char *mask, unsigned char *result, int r, int c){
	// first two loops two iterate all the data 
	for (int i=0; i<r; i++){
		for (int j=0; j<c; j++){
			int sum = 0;
			// loop on the mask 
			for (int k=0; k<MASK_SIZE; k++){
				// get index 
				int idx = (i * c + j) - MASK_RADIUS + k;
				// compute only if not at the beginning or at the end of the row
				if ((idx >= i*c) && (idx < (i+1)*c))
					sum += data[idx] * mask[k];
			}
			// gradient is interested only at the difference so compute del module of the value 
			result[i * c + j] = abs(sum);
		}
	}
}

// Vertical convolution 
// it can be obtain in two ways
/*
	1.	Changing the algorithm, it take the values in the same column and not on the same row
	2.	Because the kernel used is the same for computing the horizontal gradients the job can be done in this way 

	// -> transpose input matrix
	// -> compute horizontal convolution 
	// -> transpose the results 

	IN THE PROJECT IS APPLIED ONLY THE 1. METHOS BECAUSE THE 2 TRANPOSITIONS ARE HEAVY TO COMPUTE
*/


void convolution2DV (unsigned char *data, char *mask, unsigned char *result, int r, int c){
	for (int i=0; i<r; i++){
		for (int j=0; j<c; j++){
			int sum = 0;
			for (int k=0; k<MASK_SIZE; k++){
				int idx = (i * c + j) + (c*(k-MASK_RADIUS));
				// check idx is min and max size of the height of the image 
				if (idx >= 0 && idx < r*c)
					sum += data[idx] * mask[k];
			}
			result[i * c + j] = abs(sum);
		}
	}
}

// 
void transpose (unsigned char *data, unsigned char *transpose, const int r, const int c){
	for (int i=0; i<c; i++){
		for (int j=0; j<r; j++){
			transpose[i * r + j] = data[j * c + i];
		}
	}
}





//******************************************** GPU ********************************************// 

// Horizontal convolution 
__global__ void convolution2DH_GPU(unsigned char *data, unsigned char *result, const int mask_width, const int width){
	uint i = blockIdx.x * blockDim.x + threadIdx.x;
	
	// detect row and column 
	int m = (width + blockDim.x - 1) / blockDim.x;		// number of blocks in a row 
	int riga = blockIdx.x / m;  						// row 							 
	int colonna = i - riga * m * blockDim.x;			// column 

	// check not to be outside the range of the values 
	if (colonna >= width)
		return;

	char mask[3] = {-1,0,1};
	
	int pValue = 0;
	int mask_radius = (mask_width / 2);

	// loop on the mask 
	for (int j=0; j<mask_width; j++){
		/// compute only if not at the beginning or at the end of the row
		if ( (colonna - mask_radius + j ) >= 0	&&	(colonna - mask_radius + j) < width){
			pValue += data[riga * width + colonna - mask_radius + j] * mask[j];
		}
	}
	// not possible to use abs in cuda functions 
	result[riga * width + colonna] = pValue >= 0 ? pValue : -pValue ;
}

// Vertical convolution 
__global__ void convolution2DV_GPU(unsigned char *data, unsigned char *result, const int mask_width, const int width, const int height){
	uint i = blockIdx.x * blockDim.x + threadIdx.x;

	// detect row and column 
	int m = (width + blockDim.x - 1) / blockDim.x;
	int riga = blockIdx.x / m;
	int colonna = i - riga * m * blockDim.x;

	if (colonna >= width)
		return;

	char mask[3] = {-1,0,1};

	int pValue = 0;
	int mask_radius = (mask_width / 2);

	for (int j=0; j<mask_width; j++){
		// check to not exitmin and max values of the height of the image 
		if ( (riga - mask_radius + j) >= 0 && (riga - mask_radius + j) < height )
			pValue += data[(riga - mask_radius + j) * width + colonna] * mask[j];
	}
	result[riga * width + colonna] = pValue >= 0 ? pValue : -pValue ;
}

//******************************************** GPU shared ********************************************// 

__global__ void convolution2DH_GPU_shared(unsigned char *data, unsigned char *result, const int mask_width, const int width) {

	unsigned int idx = blockDim.x * blockIdx.x + threadIdx.x;

	__shared__ unsigned char tile[TILE_SIZE];

	int m = (width + blockDim.x - 1) / blockDim.x;
	int riga = blockIdx.x / m;
	int colonna = idx - riga * m * blockDim.x;

	int mask_radius = (mask_width / 2);

	// Fill Tile

	// 1 -> start of the tile so threadIdx.x == 0
	if (threadIdx.x == 0){		
		// 2 options
		// 1. also the column is equal 0 so we are at the beginning of the row , start value of tile 
		if (colonna == 0){
			tile[0] = 0;
		}
		// 2. we are in the middle of the row so it is needed the last value of the previous block
		else {
			tile[0] = data[riga * width + colonna - 1];
		}
	}

	// it is not the end of the line . The final value of tile is given by the first value of the next block 
	if (colonna < width && threadIdx.x == blockDim.x - 1 ){
		if (colonna < width - 1)
			tile[threadIdx.x + mask_width - 1] = data[riga * width + colonna + 1];
		// if column is equal to the end of the line set the last value of tile to zero
		if (colonna == width - 1)
			tile[threadIdx.x + mask_width - 1] = 0;
	}

	// if the end of the row is exceeded and it is also the end of the block 
	if (colonna >= width && threadIdx.x == blockDim.x - 1){
		tile[threadIdx.x + mask_width - 1] = 0;
	}	

	// if the end of the row is exceeded and it is not the end of the block
	if (colonna >= width && threadIdx.x < blockDim.x - 1){
		tile[threadIdx.x + mask_radius] = 0;
	}

	if (colonna >= width)
		return;
	
	// fill the center of tile 
	tile[threadIdx.x + mask_radius] = data[riga * width + colonna];

	// synch threads
	__syncthreads();

	// convolution 
	char mask[3] = {-1,0,1};
	int sum = 0;
	// loop on the mask and compute the value for this thread 
	for (int i = -mask_radius; i <= mask_radius; i++)
		sum += tile[threadIdx.x + mask_radius + i] * mask[i + mask_radius];

	// save module
	result[riga * width + colonna] = sum >= 0 ? sum : -sum;
}
