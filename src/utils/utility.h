/*
    UTILITY
	- not important operations like checking values and other stuff like this...
*/

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "grayscale.cu"

#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "../stb_image/stb_image_resize.h"

#define MAX_WIDTH 1200
#define MAX_HEIGHT 1600

#define SIZE_DIV 8

/*
    Check if the image entered doesn't fit the max values for the image 
    max threads = 5760000 (width * height * 3 channels)
*/
bool check_size (int w, int h){
    bool res = w*h <= MAX_HEIGHT*MAX_WIDTH ? true : false;

    if (!res)
        printf("The image entered is too big, max size = 1200x1600 \n\n");

    return res;
}

/*
    Check if the the number of channels is equal 3 RGB
*/
bool check_channels (int channel){
    bool res = channel == 3 ? true : false;

    if (!res)
        printf("Number of channels entered == %d, retry with image with 3 channels (RGB) .jpg\n\n", channel);
    
    return res;
}

/*
    Check if two arrays are equal 
*/
// the error only appears if the difference is major than 1
// because due to the casting to integer sometimes we get two different values
bool check(unsigned char *a, unsigned char *b, const int size)
{
    bool res = true;
    for (int i = 0; i < size; i++)
        if ((a[i] - b[i]) > 1)
        {
            printf("Errore qui -> %d\n", i);
            printf("%u != %u\n", a[i], b[i]);
            res = false;
        }
    return res;
}

/*
    Check if two float arrays are equal 
    For the HOG computation 
*/
bool check(float *a, float *b, const int size)
{
    bool res = true;
    for (int i = 0; i < size; i++)
        if ((a[i] - b[i]) > 1.0)
        {
            printf("Errore qui -> %d\n", i);
            printf("%f != %f\n", a[i], b[i]);
            res = false;
        }
    return res;
}

/*
    Print matrix (data in 1D, height, width)
*/
void printMat(unsigned char *data, int r, int c)
{
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            printf("%u ", data[i * c + j]);
        }
        printf("\n");
    }
}

/*
    resize Image, not used 
*/
void resizeImage(unsigned char *pixels, int &height, int &width, unsigned char *outputPixel)
{
    int new_width;
    int new_height;

    // differences
    int miss = width % SIZE_DIV != 0 ? SIZE_DIV - (width % SIZE_DIV) : 0;
    new_width = width + miss;

    miss = height % SIZE_DIV != 0 ? SIZE_DIV - (height % SIZE_DIV) : 0;
    new_height = height + miss;

    stbir_resize_uint8(pixels, width, height, 0, outputPixel, new_width, new_height, 0, 1);

    width = new_width;
    height = new_height;
}
